var WebHooks = require('node-webhooks');
var express = require('express');
var request = require('request');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var http = require('http');

var moment = require('moment');
var CONFIG = require('./config.js')();
var PATH = require('path');
var LOGGER = require('winston');

// Logging
LOGGER.level = "debug"; // Change it to your needs
var delimiter = "~#~";
LOGGER.remove(LOGGER.transports.Console); // Optional: if you don't want Logs on Console
LOGGER.add(LOGGER.transports.File, {
	filename: PATH.join("logs", "logs", "webhook.log"),
	timestamp: function() {
		return new Date().toISOString();
	},
	json: false,
	formatter: function(options) {
		return options.timestamp() + delimiter + options.level.toUpperCase() + delimiter + (options.message ? options.message : '') +
			(options.meta && Object.keys(options.meta).length ? delimiter + JSON.stringify(options.meta) : '' );
	}
});


var webHooks = new WebHooks({
	db: './webHooksDB.json', // json file that store webhook URLs 
	httpSuccessCodes: [200, 201, 202, 203, 204] //optional success http status codes 
});


var emitter = webHooks.getEmitter();

//  -------------- FETCH PAYMENT LOG API --------------
app.post('/webhook_payment_log', function (req, res) {
	var order_id = "";
	var rg_id = "";
	var payment_status = "";
	var amount = "";
	var transaction_number = "";
	var date_entered = "";
	var billdesk_retry = "";
	var bank_name = "";
	var virtual_payment = "";
	var rg_charges = {};
	var data = {};
	var url = "";

	LOGGER.info("Start the WebHook", req.body);
	var error_callback = function (code, error) {
		if (req.headers["rg-correlation-id"]) {
			res.set({
				'rg-correlation-id': req.headers["rg-correlation-id"]
			})
		}
		LOGGER.error("Error Code : ", code);
		LOGGER.error("Error Message: ", error);
		res.status(code);
		res.json({
			code: 420,
			message: error
		});
	};


	LOGGER.info("Start if condition");
	if (req.body.hasOwnProperty("order_id")) {
		if (req.body.hasOwnProperty("rg_id")) {
			if (req.body.hasOwnProperty("payment_status")) {
				if (req.body.hasOwnProperty("amount")) {
					if (req.body.hasOwnProperty("transaction_number")) {
						if (req.body.hasOwnProperty("rg_charges")) {
							if (req.body.hasOwnProperty("tenant_name")) {
								LOGGER.info("Inside if condition");
								data.order_id = req.body.order_id;
								data.rg_id = req.body.rg_id;
								data.payment_status = req.body.payment_status;
								data.amount = req.body.amount;
								data.transaction_number = req.body.transaction_number;
								var date1 = req.body.date_entered;
								LOGGER.debug("Date Recieved : " ,req.body.date_entered);
								date1 = moment(date1).format("YYYY-MM-DD HH:mm:ss");
								data.date_entered = date1;
								LOGGER.debug("Date : " ,date1);
								data.billdesk_retry = req.body.billdesk_retry;

								rg_charges.total_amount = req.body.rg_charges.total_amount;
								rg_charges.landlord_amount = req.body.rg_charges.landlord_amount;
								rg_charges.processing_fee = req.body.rg_charges.processing_fee;
								rg_charges.tax = req.body.rg_charges.tax;
								data.rg_charges = rg_charges;
								data.owner_name = req.body.owner_name;
								data.owner_email = req.body.owner_email;
								data.tenant_name = req.body.tenant_name;
								data.tenant_email = req.body.tenant_email;

								data = JSON.stringify(data);
								LOGGER.debug("Data to be sent", data);
								//Call the getUrl function from config file
								url = CONFIG.getCurrentUrl(req.body.rg_id);

								LOGGER.info("URL taken" ,url);

								//aad the webhook 
								webHooks.add('webHookCall', url).then(function () {
									res.json({
										code: 0,
										message: "OK"
									});

									LOGGER.info("Webhook call success");
								}).catch(function (err) {
									error_callback(err);
									LOGGER.error("Error on webhook call", err);
								});
								//Triggers the webhook
								webHooks.trigger('webHookCall', {
									data: data
								}, {
									header: 'headers'
								});

								emitter.on('*.success', function (shortname, statusCode, body) {
									res.status(statusCode);
									LOGGER.info("Status Code on success : " ,statusCode);
									LOGGER.error("Success on trigger", shortname);
								});

								emitter.on('*.failure', function (shortname, statusCode, body) {
									res.status(statusCode);
									LOGGER.error("Status Code on error : " ,statusCode);
									LOGGER.error("Error on trigger", shortname);
								});

							} else {
								error_callback("400", "Tenant Name is missing");
								LOGGER.error("Tenant name is missing", req.body.tenant_name);
							}
						} else {
							error_callback("400", "RG Charges is missing");
							LOGGER.error("RG charges is missing", req.body.rg_charges);
						}
					} else {
						error_callback("400", "Transaction number is missing");
						LOGGER.error("Transaction Number is missing", req.body.transaction_number);
					}
				} else {
					error_callback("400", "Amount is missing");
					LOGGER.error("Amount  is missing", req.body.amount);
				}
			} else {
				error_callback("400", "Payment Status is missing");
				LOGGER.error("Payment Status  is missing", req.body.payment_status);
			}
		} else {
			error_callback("400", "RG ID is missing");
			LOGGER.error("RG ID  is missing", req.body.rg_id);
		}
	} else {
		error_callback("400", "Order Id is missing");
		LOGGER.error("Order ID is missing", req.body.order_id);
	}

});


app.listen(9500, function () {
	console.log("Started Server on Port 9500...");
});
