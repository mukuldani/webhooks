var CONST = require('./const.js');
module.exports = function()
{
 	var webHookUrls = {
        	homigo: CONST.homigo
 	};
 	var conf = {};
 	var REGEX = {
 		reg1: /^[H]{1,3}\d+$/ //FOR Homigo
 	};
 	var url = "";
	
	conf.getCurrentUrl = function(u){
		console.log("Inside getUrl()" ,u);
		 if (u.match(REGEX.reg1)){
         		url = webHookUrls.homigo;
         	}
		return url;
 	}
	 return conf;
}
